package com.ldbsystems.xml.compare;

import java.io.InputStream;
import java.util.List;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.InputSource;

public class XMLUnitTest {

  public static void main(String[] args) throws Exception {

    // will be ok
    // assertXMLEquals("<abc attr=\"value1\" title=\"something\"></abc>", result);
  }

  @Test
  public void assertXMLEquals() throws Exception {
    InputStream inputStreamExpected = XMLUnitTest.class
        .getResourceAsStream("/compare/sample_xml_feed_enetpulse_tennis_expected.xml");
    InputStream inputStreamOriginal = XMLUnitTest.class
        .getResourceAsStream("/compare/sample_xml_feed_enetpulse_tennis_original.xml");
    // String expectedXML =
    // "<abc             attr=\"value1\"                title1=\"something\">            </abc>";
    // String actualXML = "<abc attr=\"value1\" title=\"something\"></abc>";
    XMLUnit.setIgnoreWhitespace(true);
    XMLUnit.setIgnoreAttributeOrder(true);

    DetailedDiff diff = new DetailedDiff(new Diff(new InputSource(inputStreamExpected),
        new InputSource(inputStreamOriginal)));

    List<?> allDifferences = diff.getAllDifferences();
    Assert.assertEquals("Differences found: " + diff.toString(), 0, allDifferences.size());
  }

}
