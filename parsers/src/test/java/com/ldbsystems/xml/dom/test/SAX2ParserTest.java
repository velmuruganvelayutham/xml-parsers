package com.ldbsystems.xml.dom.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ldbsystems.xml.sax.SAX2Parser;

public class SAX2ParserTest {

  SAX2Parser handler;

  @Before
  public void setUp() throws Exception {
    // sax parsing

    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
        new FileOutputStream(
            new File(
                "C:\\Users\\velmuruganv\\Downloads\\revelmurugan\\output\\sample_xml_feed_enetpulse_tennis_output_sax.xml")));
    File file = new File(
        "C:\\Users\\velmuruganv\\Downloads\\revelmurugan\\sample_xml_feed_enetpulse_tennis.xml");
    FileInputStream fileInputStream = new FileInputStream(file);
    handler = new SAX2Parser(outputStreamWriter, fileInputStream);

  }

  @After
  public void tearDown() throws Exception {

    // needs to close the resource
  }

  @Test
  public void testParse() {
    handler.doSAXParse();
  }

}
