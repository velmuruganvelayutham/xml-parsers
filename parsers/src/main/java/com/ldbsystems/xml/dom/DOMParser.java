package com.ldbsystems.xml.dom;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMParser {

  OutputStreamWriter osw;
  FileInputStream is;

  /**
   * @param osw
   * @param is
   *          TODO
   */
  public DOMParser(OutputStreamWriter osw, FileInputStream is) {
    super();
    this.osw = osw;
    this.is = is;
  }

  private void printNote(NodeList nodeList) {

    for (int count = 0; count < nodeList.getLength(); count++) {

      Node tempNode = nodeList.item(count);

      // make sure it's element node.
      if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

        // get node name and value
        // System.out.println("\nNode Name =" + tempNode.getNodeName() + " [OPEN]");
        // System.out.println("Node Value =" + tempNode.getTextContent());
        write(" " + tempNode.getNodeName() + ": \n");
        // write(" " + tempNode.getTextContent());

        if (tempNode.hasAttributes()) {

          // get attributes names and values
          NamedNodeMap nodeMap = tempNode.getAttributes();

          for (int i = 0; i < nodeMap.getLength(); i++) {

            Node node = nodeMap.item(i);
            // System.out.println("attr name : " + node.getNodeName());
            // System.out.println("attr value : " + node.getNodeValue());
            write("\t" + node.getNodeName() + ":");
            write(" " + node.getNodeValue() + "\n");

          }

        }

        if (tempNode.hasChildNodes()) {

          // loop again if has child nodes
          printNote(tempNode.getChildNodes());

        }

        // System.out.println("Node Name =" + tempNode.getNodeName() + " [CLOSE]");
        // write(" " + tempNode.getNodeName() + " \n");
        write("\n");
      }

    }

  }

  public void doDOMParse() {
    try {

      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(is);

      if (doc.hasChildNodes()) {

        printNote(doc.getChildNodes());

      }
      osw.flush();
      osw.close();
      is.close();

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void write(String value) {
    try {
      osw.write(value);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

}
